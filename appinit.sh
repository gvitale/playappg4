#!/bin/sh

aws s3 cp Dockerrun.aws.json s3://nxgeneddocker/playappg4/1.0/

eb init \
   -a playappg4 \
   -l 1.0 \
   -e playappg4-env \
   --region us-east-1 \
   -t WebServer::Standard::1.0 \
   -s "64bit Amazon Linux 2014.09 v1.0.10 running Docker 1.3.2"

aws elasticbeanstalk \
     create-application-version \
     --application-name playappg4 \
     --version-label 1.0 \
     --source-bundle S3Bucket="nxgeneddocker",S3Key="playappg4/1.0/Dockerrun.aws.json"

