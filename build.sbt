enablePlugins(JavaAppPackaging, DockerPlugin)

packageName in Docker          := "playappg4"

version in Docker              := "1.0"

dockerBaseImage                := "dockerfile/java:oracle-java7"

dockerRepository               := Some("nxgened")

dockerExposedPorts in Docker   := Seq(9000)

dockerExposedVolumes in Docker := Seq("/opt/docker/logs")

dockerUpdateLatest             := true

daemonUser in Docker           := "root"

name                           := "playappg4"

version                        := "1.0"

bashScriptConfigLocation       := Some("/opt/docker/conf/etc-default")

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.10.4"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  ws
)

