package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.data.validation.Constraints
import java.util.Date

object Application extends Controller with Constraints{
  val userFormConstraints = Form(
    mapping(
      "name" -> nonEmptyText,
      "username" -> nonEmptyText(6),       // requires a minimum of six characters
      "password" -> text(minLength = 10),  // requires a minimum of ten characters
      "age" -> number(min = 0, max = 100),
      "birddate" -> date("yyyy-MM-dd"),
      "email" -> email,
      "interests" -> list(text),
      "street" ->  text.verifying(nonEmpty), //Validation pachage: simple non empty text
      "cellphone" -> number.verifying("It is not a valid Cell Phone - it should has 10 digits!", CheckCellPhone(_) == true)//Validation pachage: Cutom method: requires a minimum of ten characters
    )(UserData.apply)(UserData.unapply)
  )

  def index = Action {
    Ok(views.html.user(userFormConstraints))
  }

  def validate = Action {
    implicit request => userFormConstraints.bindFromRequest.fold(
      formWithErrors => {
        // binding failure, you retrieve the form containing errors:
        BadRequest(views.html.user(formWithErrors))
      },
      userData => {
        /* binding success, you get the actual value. */
        /*val newUser = models.User(userData.name, userData.age)
        val id = models.User.create(newUser)
        Redirect(routes.Application.home(id))*/
        Ok(views.html.index("Success!!"))
      }
    )
  }

  def CheckCellPhone(cellphone: Int) = {
    if(cellphone.toString.length == 10){
      true
    }else{
      false
    }
  }

}

case class UserData (name: String, username: String, password: String, age: Int, birddate: java.util.Date, email: String, interests: List[String], street: String, cellphone: Int){}

